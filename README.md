# Dash Dash Alias

`--/` (or dash dash alias) is an attempt at resolving dependency hell in JavaScript projects.

This project showcases how to set up various environments so that `require('--/file')` or `import '--/file'` resolves to your project source directory.

## The Problem

```js
import { hello } from '../../greetings'
import { shout } from '../../../utils/str'
import { world } from '../../../api'

export default shout(hello(world))
```

Or,

```js
const { hello } = require('../../greetings')
const { shout } = require('../../../utils/str')
const { world } = require('../../../api')

module.exports = shout(hello(world))
```

### The Other Problem

Node, Rollup, React...

There are a multitude of packages and scripts that run and build JavaScript projects. There's no "one way" to configure all of them to use an alias so each implementation requires a different set of directions. This is a public work in progress and community help will be needed to create guides for all types of projects. Any help is greatly appreciated!

## The Solution

```js
import { hello } from '--/components/greetings'
import { shout } from '--/utils/str'
import { world } from '--/api'

export default shout(hello(world))
```

Or,

```js
const { hello } = require('--/components/greetings')
const { shout } = require('--/utils/str')
const { world } = require('--/api')

module.exports = shout(hello(world))
```

### The Actual Solution

The following guides on implementing aliases have been published:

-   [Node.js](https://gitlab.com/dashdashalias/node)
-   [Parcel](https://gitlab.com/dashdashalias/parcel)
-   [Solid](https://gitlab.com/dashdashalias/solid)

If you are feeling particularly generous, please feel free to request access to the group or create an issue with a list of steps to get aliases working with a given project.

## Some Reasoning

The package names `app`, `lib`, and `src` are all available on NPM as individual packages. Although nothing stops us from using these names locally for our development to resolve source directories, in the process of addressing one issue, we technically create a conflicting NPM package. Developers working on the projects must also know it's an alias to the source directory and not a NPM package. Having a package name dedicated to dependency hell resolution solves both of these problems.

The package name `--` is valid although unpublishable due to the package `-` existing. We don't specify the directory separator when defining the alias so you can traverse the source directory tree normally in your imports/requires.
