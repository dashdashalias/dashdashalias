#!/usr/bin/env node

// Our depdency name
const DASHDASH = 'dashdashalias'

// Console colour helpers
// https://stackoverflow.com/questions/9781218/how-to-change-node-jss-console-font-color
const chalk = { cyan: '\x1b[36m', green: '\x1b[32m', none: '\x1b[0m', red: '\x1b[31m', yellow: '\x1b[33m' }

// Only allow install on Node
if (typeof window === 'undefined' && typeof process === 'object') install()

/**
 * Attempts to install the "--" alias.
 *
 * @return {void}
 */
async function install () {
  if (!hasArg()) error('missing source directory argument')

  // Package imports
  const fs = require('fs/promises')
  const path = require('path')

  // Get the current directory tree to work with
  const cwd = process.cwd()

  // Resolve paths we're working with
  const paths = {
    alias: path.resolve(cwd, 'node_modules', '--'),
    node: path.resolve(cwd, 'node_modules'),
    pkg: path.resolve(cwd, 'package.json'),
    source: path.resolve(cwd, process.argv[2])
  }

  // If there's an existing alias, we'll want to skip it in finalize
  let clean

  // Save package.json data so it's only required once
  let pkg

  // Print install message
  print()
  print('%c install %c"--/" (dash dash alias) >>> %s/', chalk.cyan, chalk.none, paths.source)

  // Validate a bunch of things before trying to install the alias
  if (!hasPkg()) error('invalid package.json found in %s/', cwd)
  if (!hasDep()) error('missing package.json dependency %s', DASHDASH)
  if (!hasInstall()) warn('no install script found calling %s', DASHDASH)
  if (!await canRead()) error('could not read source directory %s/', paths.source)
  if (!await canWrite()) error('could not create node_modules folder')
  if (!await isClean() && !await isSame()) error('existing alias in node_modules does not point to %s/', paths.source)
  if (!await finalize()) error('could not create alias in node_modules')

  // Print success message
  print('%c success %c"--/" >>> %s/', chalk.green, chalk.none, paths.source)
  print()

  /**
   * Checks if the source directory exists.
   *
   * This function returns undefined when no directory exists.
   *
   * @return {Promise<Boolean|undefined>}
   */
  async function canRead () {
    // The access method returns undefined or throws if the file/directory can't be read
    return suppress(async () => !await fs.access(paths.source))
  }

  /**
   * Checks if we can create and/or read the node_modules folder.
   *
   * This function returns undefined when failing to create the directory.
   *
   * @return {Promise<Boolean|undefined>}
   */
  async function canWrite () {
    // Attempt to create the node_modules folder while ignoring errors like EEXISTS
    await suppress(() => fs.mkdir(paths.node))

    // Using access to make sure the directory was created
    return suppress(async () => !await fs.access(paths.node))
  }

  /**
   * Creates the symlink to the source directory if it doesn't already exists.
   *
   * @return {Promise<Boolean>}
   */
  async function finalize () {
    // Not clean means the existing symlink points to the source location, or create it
    return !clean || suppress(async () => !await fs.symlink(paths.source, paths.alias))
  }

  /**
   * Checks if we received the minimum required arguments.
   *
   * @return {Boolean}
   */
  function hasArg () {
    // $ /usr/bin/env node source-directory [...discarded]
    return process.argv.length > 2
  }

  /**
   * Checks if our dashdashalias package is in the dependencies.
   *
   * @return {Boolean}
   */
  function hasDep () {
    // Must be a dependency in node and not a dev dependency
    return typeof pkg.dependencies === 'object' &&
      pkg.dependencies[DASHDASH] !== undefined
  }

  /**
   * Checks if there's an install script calling dashdashalias.
   *
   * @return {Boolean}
   */
  function hasInstall () {
    // Regex used to find install script
    const install = /install/
    const dashdash = new RegExp(DASHDASH)

    // Attempts to find the word "install" in script keys and "dashdashalias" in their values
    return typeof pkg.scripts === 'object' &&
      Object.keys(pkg.scripts).find((script) => {
        return install.test(script) && dashdash.test(pkg.scripts[script])
      }) !== undefined
  }

  /**
   * Checks if the working directory contains a package.json file.
   *
   * @return {Boolean}
   */
  function hasPkg () {
    try {
      // Attempting to load the package.json and read its dependencies
      pkg = require(paths.pkg)

      // The above would throw an error if package.json didn't exist or was malformed
      return true
    } catch (_) {
      // Failed to read package.json
      return false
    }
  }

  /**
   * Checks if the node_modules folder contains an existing "--" alias.
   *
   * @return {Promise<Boolean>}
   */
  async function isClean () {
    // Keep track of the alias for finalize
    clean = await suppress(async () => !await fs.access(paths.alias)) === undefined

    // If the alias exists, suppress will return true, but we expect access to throw
    return clean
  }

  /**
   * Checks if the existing "--" alias in node_modules points to the same source.
   *
   * @return {Promise<Boolean>}
   */
  async function isSame () {
    // Ensure the existing alias is a symbolic link and that it resolves to the same source
    return (await fs.lstat(paths.alias)).isSymbolicLink() &&
      path.resolve(paths.node, await fs.readlink(paths.alias)) === paths.source
  }
}

/**
 * Helper function that prints errors and stops the current application.
 *
 * @param  {String}   message Error message to print
 * @param  {String[]} strings Variables to print
 * @return {void}
 */
function error (message, ...strings) {
  // Show "error" in red and everything else normally
  print('%c   error %c"--/" ' + message, chalk.red, chalk.none, ...strings)
  print()

  // Stop process with a non-zero exit code
  process.exit(1)
}

/**
 * Helper function that simplifies printing variables in messages.
 *
 * @param  {String}   message Message to replace %s, %d, and %c then print
 * @param  {String[]} strings Variables to print
 * @return {void}
 */
function print (message, ...strings) {
  // Don't try to replace message contents without variables
  if (strings.length) {
    // Replace each %[sdc] found by matching the variables index
    console.log(message.replace(/%[sdc]/g, () => strings.shift()) + chalk.none)
  } else {
    // Nothing to replace or printing an empty line
    console.log(message || '')
  }
}

/**
 * Helper function to suppress all thrown errors in the provided callback.
 *
 * @param  {Function}   callback Function that might throw
 * @return {mixed|void}          Returns the value of the callback if any
 */
async function suppress (callback) {
  try {
    // Access value once
    const result = callback()

    // Wait for promises to resolve before returning them
    return result instanceof Promise ? await result : result
  } catch (_) {
    // Ignore all thrown errors
  }
}

/**
 * Helper function that prints warning messages.
 *
 * @param  {String}   message Warning message to print
 * @param  {String[]} strings Variables to print
 * @return {void}
 */
function warn (message, ...strings) {
  // Print "warning" in yellow and everything else normally
  print('%c warning %c"--/" ' + message, chalk.yellow, chalk.none, ...strings)
}
